global without sharing class VPL_GetInventoryDetails implements vlocity_cmt.VlocityOpenInterface {

    public Boolean invokeMethod(String methodName, Map < String, Object > input, Map < String, Object > output, Map < String, Object > options) {

        if (methodName.containsIgnoreCase('getNumberList')) {

            getServiceIdentifier(input, output, options);

        }

        if (methodName.containsIgnoreCase('getSimDetails')) {

            getSimDetails(input, output, options);

        }

        return true;

    }



    //Return Number Inventory Details

    public void getServiceIdentifier(Map < String, Object > input, Map < String, Object > output, Map < String, Object > options) {

        //Wrapper Inventory List declaration

        List < numberinventoryWrapper > dataList = new List < numberinventoryWrapper > ();

        Map < String, Object > serviceIdentifierDets = (Map < String, Object > ) input.get('getServiceIdentifierList');

        Set < String > MSISDNs = (Set < String > ) serviceIdentifierDets.keyset();

        List < String > MSISDNList = new List < String > ();

        List < Object > MSISDNsValues = (List < Object > ) serviceIdentifierDets.values();



        //Convert Set to List

        if (MSISDNs.size() > 0) {

            For(String ms: MSISDNs) {

                MSISDNList.add(ms);

            }

        }



        if (MSISDNs.size() > 0) {

            For(Integer i = 0; i < MSISDNs.size(); i++) {

                numberinventoryWrapper iw = new numberinventoryWrapper();

                String s = JSON.serialize(MSISDNsValues[i]);

                iw = (numberinventoryWrapper) JSON.deserialize(s, numberinventoryWrapper.class);

                iw.MSISDN = MSISDNList[i];

                dataList.add(iw);

            }

        }



        output.put('dataList', dataList);



    }



    //Return SIM card Inventory Details

    public void getSimDetails(Map < String, Object > input, Map < String, Object > output, Map < String, Object > options) {

        siminventoryWrapper simwrap = new siminventoryWrapper();

        Map < String, Object > simDetails = (Map < String, Object > ) input.get('simDetails');

        Set < String > IMSIs = (Set < String > ) simDetails.keyset();

        List < String > iMSIList = new List < String > ();

        List < Object > simValuesObjs = (List < Object > ) simDetails.values();

        //Convert Set to List

        if (IMSIs.size() > 0) {

            For(String imsiObj: IMSIs) {

                iMSIList.add(imsiObj);

            }

        }

        /*

        if (IMSIs.size() > 0) {



            String s = JSON.serialize(simValuesObjs[0]);

            simwrap = (siminventoryWrapper) JSON.deserialize(s, siminventoryWrapper.class);

            simwrap.IMSI = iMSIList[0];

        }

       */ 

             //Return more than one sim details(ICCID,IMSI) record.

             List<siminventoryWrapper>simDetailslist=new List<siminventoryWrapper>();

             if (IMSIs.size() > 0)

             {

               For(Integer i = 0; i < IMSIs.size(); i++) 

               {

                siminventoryWrapper iw = new siminventoryWrapper();

                String s1 = JSON.serialize(simValuesObjs[i]);

                iw = (siminventoryWrapper) JSON.deserialize(s1, siminventoryWrapper.class);

                iw.IMSI = iMSIList[i];

                simDetailslist.add(iw);

              }

            }

         output.put('simDetailslist', simDetailslist);

    }



    //Intializing all the wrapper class

    public class numberinventoryWrapper {

        public String MSISDN;

        public String Price;

        public String Is_Premium;

        public String Status;

        public String Type;

    }

    public class siminventoryWrapper {

      public String Type;

      public String IMSI;

      public String ICCID;

      public String Serial_Number;

      public String Status ;

    }



}