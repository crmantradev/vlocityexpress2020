global with sharing class AbsServiceHandler4 implements vlocity_cmt.VlocityOpenInterface{
    /*********************************************************************************************
     * @Class AbsServiceHandler4
     * @Description Abstraction Service to manage
     *              - getFieldDataType
     *              - SetValue Tech
     * @Created Alain KAMENI, May 2020
     **********************************************************************************************/
    static String METHOD_NAME = '';
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean result = true;
        String errormessage ='';
        try
        {
            METHOD_NAME = methodName;
            if (methodName.equals('setValue')){
                setValue(inputMap,outMap,options);  
            }else if (methodName.equals('getFieldDataType')){
                getFieldDataType(inputMap,outMap,options);  
            }else{
                result = false;
            } 
        } 
        catch(Exception e)
        {
            System.debug('invokeMethod ERROR is ->: '+e.getMessage());
            System.debug('invokeMethod -> exception: '+e.getStackTraceString());
            result = false;
            Map<String, Object> errorResult = AbsServiceHandler.returnErrorResult('technicalError',METHOD_NAME,e);
            outMap.put('ServiceError',errorResult); 
        }
        return result;
    }
        global static void getFieldDataType(Map<String, Object> inputMap, Map<String, Object> outMap,Map<String,Object> options)
    {
        try{    
            Map <String,String> mapDataTypeByFieldName = new Map <String,String>();
            ID objectId = inputMap.get('ObjectId') =='' || inputMap.get('ObjectId') ==null?null:(ID)inputMap.get('ObjectId');
            String sObjectName = objectId==null?null:String.valueOf((objectId).getSObjectType());
            if(sObjectName == 'Order') sObjectName= 'OrderItem';
            else if (sObjectName == 'Quote') sObjectName= 'QuoteLineItem';
            else if (sObjectName == 'Opportunity') sObjectName= 'OpportunityLineItem';
            
            if(sObjectName ==null) sObjectName = inputMap.get('sObjectName') ==null ||inputMap.get('sObjectName') ==''?null:(String)inputMap.get('sObjectName');
            Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sObjectName).getDescribe().SObjectType.getDescribe().fields.getMap();
            
            for(Schema.SObjectField field : fields.values()){
                Schema.DescribeFieldResult fieldResult = field.getDescribe();
                if(fieldResult.isUpdateable()){
                    string fieldName= field.getDescribe().getName();
                    Schema.DisplayType fielddataType = fields.get(fieldName).getDescribe().getType();
                    string FieldType = string.valueOf(fielddataType); 
                    mapDataTypeByFieldName.put(fieldName,FieldType);
                }
            }
            if(!mapDataTypeByFieldName.isEmpty()){
                outMap.put('mapDataTypeByFieldName',mapDataTypeByFieldName);
            }
            outMap.put('SObject',sObjectName);
        }catch (Exception e){
            System.debug('invokeMethod ERROR is ->: '+e.getMessage());
            System.debug('invokeMethod -> exception: '+e.getStackTraceString());
            Map<String, Object> errorResult = AbsServiceHandler.returnErrorResult('technicalError',METHOD_NAME,e);
            outMap.put('ServiceError',errorResult); 
        }
    }
    
    private static void setItemsByContextId (String updateType, String fieldName, Object fieldValue, String fieldDataType, Set<ID> setItemId, List<SObject> listItems,String jsonAttributeValue,Schema.SObjectType globalDescribeSObject){
        List<SObject> outListItems = new List<SObject>();
        Boolean duplicateOli;
        for(ID sId : setItemId){
            SObject oli;
            duplicateOli = false;
            for (SObject oItemField : listItems){
                if( sId == oItemField.Id){
                    oli = oItemField;
                    duplicateOli = true;
                    break;
                }
            }
            if(!duplicateOli){
                oli = globalDescribeSObject.newSObject();
                oli.Id = sId; 
            }  

            if(fieldName != ''){
                if(updateType == 'FieldTech' || updateType == 'AttributeFieldTech'){
                   assynFieldValue (oli, fieldName, fieldValue, fieldDataType);
                } 
            }
            if(!duplicateOli){
               listItems.add(oli);
            }   
        }
    }
    /* Assyn value to a one Field */
    public static void assynFieldValue (SObject oliElt, String fieldName, Object fieldvalue, String fieldDataType){
        
        if(fieldDataType == null || fieldDataType ==''){ //Field Data Type not provided or not got automatically
            if(((String)fieldvalue).isNumeric())
                oliElt.put(fieldName, Integer.ValueOf(fieldvalue));
            else if(fieldvalue instanceOf Date)
                oliElt.put(fieldName, Date.ValueOf(fieldvalue));
            else
                oliElt.put(fieldName, (String)fieldvalue);
        }else{
            switch on fieldDataType{
                when 'CURRENCY'{
                    oliElt.put(fieldName, Integer.ValueOf(fieldvalue));
                }
                when 'DOUBLE'{
                    oliElt.put(fieldName, Double.ValueOf(fieldvalue));
                }
                when 'DATE'{
                    //oliElt.put(fieldName, Date.ValueOf(fieldvalue)); //Bug 2020-07-27: comment
                    oliElt.put(fieldName, Date.valueOf((String)fieldvalue)); //Bug 2020-07-27: Add (String)
                }
                when 'DATETIME'{
                    //oliElt.put(fieldName, Datetime.ValueOf(fieldvalue)); //Bug 2020-07-27: comment
                    oliElt.put(fieldName, DateTime.valueOf((String)fieldvalue)); //Bug 2020-07-27: Add (String)
                }
                when else{ //Convert to String
                    oliElt.put(fieldName, (String)fieldvalue);
                }                
            }
        }
        
    }
    //EVOL 2
    /*SetValue: SetFieldValue + Set Attributes Technically(not using API) + Field+Set Attributes Technically(not using API)*/
    global static void setValue(Map<String, Object> inputMap, Map<String, Object> outMap,Map<String,Object> options)
    {
        try{              
            Map<String,Object> inputWrap = new Map<String,Object>();
            String sObjectApiName = inputMap.get('SObject') ==null || inputMap.get('SObject') ==''?'OrderItem':(String)inputMap.get('SObject'); //API Name of SObject to process (OrderItem (default), Asset, ...)       
            String updateType = (String)inputMap.get('UpdateType');
            String setFieldMode =(inputMap.get('setFieldMode')==null || inputMap.get('setFieldMode')=='') ? 'default': (String)inputMap.get('setFieldMode');
            String getFieldDataType =(inputMap.get('getFieldDataType')==null || inputMap.get('getFieldDataType')=='') ? 'default': (String)inputMap.get('getFieldDataType'); 
            String setAttributeMode =(inputMap.get('setAttributeMode')==null || inputMap.get('setAttributeMode')=='') ? 'default': (String)inputMap.get('setAttributeMode'); 
            String attr_info_Tech = (String)inputMap.get('attr_info');
            List<Object> excludedListItemId = new List<Object>();
            Schema.SObjectType globalDescribeSObject= Schema.getGlobalDescribe().get(sObjectApiName);
            if(inputMap.get('excludedListItemId')!=null || inputMap.get('excludedListItemId')!=''){
                if(inputMap.get('excludedListItemId') instanceof List<Object>){
                    excludedListItemId = (List<Object>)inputMap.get('excludedListItemId');
                }else excludedListItemId.add(inputMap.get('excludedListItemId'));
            }
            List <Object> fieldValueList = new List <Object> ();
            List<Object> attributeSingleValueList = new List<String>();
            Map<Id,SObject> mapItemById = (Map<Id,SObject>)inputMap.get('mapItemById'); //
            Map <String,String> mapDataTypeByFieldName = new Map <String,String> (); //EVOL #2 Field Data Type
            
            if(inputMap.get('mapDataTypeByFieldName') !=null && inputMap.get('mapDataTypeByFieldName')!=''){
                mapDataTypeByFieldName = (Map <String,String>)inputMap.get('mapDataTypeByFieldName');
            }
            if(inputMap.get('attributeSingleValueList') !=null && inputMap.get('attributeSingleValueList')!=''){
               if(inputMap.get('attributeSingleValueList') instanceof List<Object>){
                    attributeSingleValueList = (List<Object>)inputMap.get('attributeSingleValueList');
               }else attributeSingleValueList.add(inputMap.get('attributeSingleValueList'));
            }            
            SObject oli;
            Map <ID, Set<ID>> mapItemsByContId  = new Map <ID, Set<ID>> (); //EVOL#3
            mapItemsByContId = (Map <ID, Set<ID>>)inputMap.get('mapItemsByContextId');
            Boolean duplicateOli = false;
            List<SObject> listItems = new List<SObject>();
            String sFieldDataType; //EVOL#2 FieldDataType
                
            if(inputMap.get('FieldsValues') != null && inputMap.get('FieldsValues') != ''){
                    if(inputMap.get('FieldsValues') instanceof List<Object>){
                        fieldValueList = (List<Object>)inputMap.get('FieldsValues');
                    }else{
                        fieldValueList.add(inputMap.get('FieldsValues'));
                    }
                    if(updateType == 'FieldTech' || updateType == 'AttributeFieldTech'){
                        if(setFieldMode == 'all'){ //setFieldMode: All
                            Boolean fieldToUpdate = false;
                            for(ID oiId : (Set<ID>)inputMap.get('setCartItemIds')){
                                SObject oliElt =  globalDescribeSObject.newSObject();
                                oliElt.Id = oiId;
                                for (Object data : fieldValueList){
                                    Map<String,Object> field = (Map<String,Object>)data;
                                    if(!(field.get('Value') instanceof List<Object>)){
                                      fieldToUpdate = true;
                                      sFieldDataType = getFieldDataType == 'auto'?mapDataTypeByFieldName.get((String)field.get('Name')):(String)field.get('TargetDataType');
                                      assynFieldValue (oliElt, (String)field.get('Name'), field.get('Value'), sFieldDataType);
                                    }
                                }
                                if(fieldToUpdate) listItems.add(oliElt);// Add to list to update
                            }
                        }
                    if (setFieldMode =='default'){
                        for (Object data : fieldValueList){
                            Map<String,Object> field = (Map<String,Object>)data;
                               if(field.get('Value') instanceof List<Object>){
                                for (Object objVal : (List<Object>)field.get('Value')){
                                    Map <String,Object> mapVal = (Map <String,Object>)objVal;
                                    if (mapItemsByContId.get((String)mapVal.get('ContextId'))!=null){
                                        sFieldDataType = getFieldDataType == 'auto'?mapDataTypeByFieldName.get((String)field.get('Name')):(String)field.get('TargetDataType');
                                        setItemsByContextId (updateType,(String)field.get('Name'),mapVal.get('Value'),sFieldDataType,mapItemsByContId.get((String)mapVal.get('ContextId')),listItems,null,globalDescribeSObject);
                                    }   
                                  }
                                }else{
                                    //**Update a listContext with same value
                                    for(String contextId : mapItemsByContId.keySet()){
                                        sFieldDataType = getFieldDataType == 'auto'?mapDataTypeByFieldName.get((String)field.get('Name')):(String)field.get('TargetDataType');
                                        setItemsByContextId (updateType,(String)field.get('Name'),field.get('Value'),sFieldDataType,mapItemsByContId.get(contextId),listItems,null,globalDescribeSObject);
                                    }
                                    for (SObject oItemField : listItems){
                                        if(inputMap.get('ItemId') !=null && inputMap.get('ItemId') !='' && oItemField.Id == (String)inputMap.get('ItemId')){ //EVOL 4: 2020-05-14: Check ItemId to avoid wrong Id (empty value)
                                            oli = oItemField;
                                            duplicateOli = true;
                                            break;
                                        }
                                    }
                                    if(!duplicateOli && inputMap.get('ItemId')!=null && inputMap.get('ItemId')!=''){ //EVOL 4: 2020-05-14: START Review with condition on ItemId
                                        oli = globalDescribeSObject.newSObject();
                                        oli.Id =(String)inputMap.get('ItemId');
                                    }
                                    if(inputMap.get('ItemId')!=null && inputMap.get('ItemId')!=''){
                                        sFieldDataType = getFieldDataType == 'auto'?mapDataTypeByFieldName.get((String)field.get('Name')):(String)field.get('TargetDataType');
                                        assynFieldValue (oli, (String)field.get('Name'), field.get('Value'), sFieldDataType);
                                    }
                                if(!duplicateOli && oli !=null){
                                    listItems.add(oli);
                                } else duplicateOli = false;
                            } 
                            // 
                            }
                            
                        }
                    
                    } 
                    
                }
                                                    
            if(updateType == 'AttributeTech' || updateType == 'AttributeFieldTech'){
                if(setAttributeMode == 'all'
                  || (!mapItemsByContId.isEmpty()) //EVOL#3
                  ){       
                    Set<ID> setAllItemsId = new Set<ID>();
                    if(setAttributeMode == 'all'){
                          setAllItemsId= (Set<ID>)inputMap.get('setCartItemIds');
                    }else if(!mapItemsByContId.isEmpty()){  //EVOL#3
                        for(ID contId : mapItemsByContId.keySet()){ 
                            setAllItemsId.addAll(mapItemsByContId.get(contId)); //EVOL #3: Update attribute with same value for a list of contextId
                        }
                    }
                    for(ID oiId : setAllItemsId){
                       // OrderItem oliElt = new OrderItem(); //JUNE 2020 - Comment
                        SObject oliElt; //JUNE 2020 - Add
                        if(updateType == 'AttributeTech'){
                            oliElt = globalDescribeSObject.newSObject();
                        }else if(updateType == 'AttributeFieldTech'){
                            for (SObject oItemField : listItems){
                                if(oItemField.Id == oiId){
                                    oliElt = oItemField;
                                    duplicateOli = true;
                                    break;
                                }
                            }
                            if(!duplicateOli){
                                oliElt = globalDescribeSObject.newSObject();
                            }
                        }
                        if(mapItemById !=null){
                            preSeItemAttributeTech ( mapItemById.get(oiId), oliElt, oliElt, sObjectApiName);
                            if(!attributeSingleValueList.isEmpty()){
                                for(Object attrfound : attributeSingleValueList){
                                  if(oliElt.get('vlocity_cmt__JSONAttribute__c') !=null && ((String)oliElt.get('vlocity_cmt__JSONAttribute__c')).contains((String)attrfound)
                                      || oliElt.get('vlocity_cmt__AttributeSelectedValues__c') !=null && ((String)oliElt.get('vlocity_cmt__AttributeSelectedValues__c')).contains((String)attrfound)){ //only process order item which has the attribute to update                                    
                                      String invokejson = (String)AbsServiceHandler.invokejsonSupport(oliElt, attr_info_Tech);
                                    if(invokejson !=null){ //avoid updating json attribute to null
                                        if(oliElt.get('vlocity_cmt__JSONAttribute__c') !=null  && oliElt.get('vlocity_cmt__AttributeSelectedValues__c') ==null){ //v1 Attribute model
                                            oliElt.put('vlocity_cmt__JSONAttribute__c',invokejson);
                                        }else if(oliElt.get('vlocity_cmt__AttributeSelectedValues__c') !=null){ //v2 Attribute model
                                            oliElt.put('vlocity_cmt__AttributeSelectedValues__c',invokejson);
                                        }
                                        if(!duplicateOli){
                                            listItems.add(oliElt);// Add to list to update
                                         }else duplicateOli = false;    
                                        break;
                                      }
                                        
                                    }//
                                }
                            } //end if attr
                        }
                        
                    }
                }
                //else if(setAttributeMode == 'default'){  //EVOL#3 TO CHECK (comment) issue with listContext
                if(setAttributeMode == 'default'){    
                    if(!attributeSingleValueList.isEmpty()){
                        //To manage duplicate
                        for (SObject oItemField : listItems){
                            if(oItemField.Id == (String)inputMap.get('ItemId')){
                                oli = oItemField;
                                duplicateOli = true;
                                break;
                            }
                        }
                        if(!duplicateOli){
                            oli = globalDescribeSObject.newSObject();
                        } 
                        
                        if(mapItemById.get((String)inputMap.get('ItemId'))!=null){
                            preSeItemAttributeTech (mapItemById.get((String)inputMap.get('ItemId')), oli, oli, sObjectApiName);
                            if(attr_info_Tech !=null && attr_info_Tech !=''){
                                String invokejson1 = (String)AbsServiceHandler.invokejsonSupport(oli, attr_info_Tech);
                                if(invokejson1 !=null){ //avoid updating json attribute to null
                                    if(oli.get('vlocity_cmt__JSONAttribute__c') !=null && oli.get('vlocity_cmt__AttributeSelectedValues__c') ==null){ //v1 Attribute model
                                        oli.put('vlocity_cmt__JSONAttribute__c',invokejson1);
                                    }else if(oli.get('vlocity_cmt__AttributeSelectedValues__c') !=null){ //v2 Attribute model
                                        oli.put('vlocity_cmt__AttributeSelectedValues__c',invokejson1);
                                    }
                                }
                            }
                        }else oli=null;
                        
                        if(!duplicateOli && oli!=null){
                            listItems.add(oli);
                        }
                    }
                    //end manage duplicate
                    if(inputMap.get('mapAttributesGroupItemId') !=null && inputMap.get('mapAttributesGroupItemId')!=''
                     && !mapItemsByContId.isEmpty()
                      ){
                        Map <String,Map<String,Object>> mapAttributesGroupItemId = (Map <String,Map<String,Object>>)inputMap.get('mapAttributesGroupItemId');
                          if(!mapAttributesGroupItemId.isEmpty()){
                            for(String contId : mapAttributesGroupItemId.keySet()){
                                duplicateOli = false;
                                Map <String,Object> listAttrContext = mapAttributesGroupItemId.get(contId); //get list of Attributes for a given context
                                if(mapItemsByContId.get(contId) !=null){
                                    for(ID oiId : mapItemsByContId.get(contId)){
                                    SObject oliElt = globalDescribeSObject.newSObject();
                                    //manage Duplicate
                                    for (SObject oItemField : listItems){
                                        if(oItemField.Id == oiId){
                                            oliElt = oItemField;
                                            duplicateOli = true;
                                            outMap.put('oliElt1',oliElt);
                                            break;
                                        }
                                     }
                                    // End manage duplicate
                                    preSeItemAttributeTech (mapItemById.get(oiId), oliElt, oliElt, sObjectApiName);
                                     if(!listAttrContext.isEmpty()){
                                        for(Object attrfound : listAttrContext.keySet()){
                                            if(oliElt.get('vlocity_cmt__JSONAttribute__c') !=null && ((String)oliElt.get('vlocity_cmt__JSONAttribute__c')).contains((String)attrfound)
                                      || oliElt.get('vlocity_cmt__AttributeSelectedValues__c') !=null && ((String)oliElt.get('vlocity_cmt__AttributeSelectedValues__c')).contains((String)attrfound)){ //only process order item which has the attribute to update                                    
                                      String invokejson = (String)AbsServiceHandler.invokejsonSupport(oliElt, json.serialize(listAttrContext));
                                      if(invokejson !=null){ //avoid updating json attribute to null
                                            if(oliElt.get('vlocity_cmt__JSONAttribute__c') !=null  && oliElt.get('vlocity_cmt__AttributeSelectedValues__c') ==null){ //v1 Attribute model
                                                oliElt.put('vlocity_cmt__JSONAttribute__c',invokejson);
                                            }else if(oliElt.get('vlocity_cmt__AttributeSelectedValues__c') !=null){ //v2 Attribute model
                                                oliElt.put('vlocity_cmt__AttributeSelectedValues__c',invokejson);
                                            }
                                            if(!duplicateOli){
                                                listItems.add(oliElt);// Add to list to update
                                             }else duplicateOli = false;    
                                            break;
                                       }
                                    }//
                                        }//end for
                                     } //end if    
                            }
                                }
                                
                            }
                      } 
                }
            }
            }
            
            if(listItems.size()>0){
                List<SObject> listItemsClone = listItems.clone();
                for(SObject oId : listItemsClone){ //EVOL#3: exclude line items in the excludeListGroupItemId
                    if(excludedListItemId.contains(oId.Id)){
                       listItems.remove(listItems.indexOf(oId));
                       continue;
                    }
                 }
                outMap.put('listItems',listItems);
                for(SObject oId : listItems){ //update all lineItems
                    if((mapItemById.get(oId.Id)).get('vlocity_cmt__Action__c') == 'Existing' && oId.get('vlocity_cmt__Action__c')==null){//update Action only when "Action" current value is "Existing" and "Action" field is not in input of line Items to update.
                        oId.put('vlocity_cmt__Action__c','Change'); // update the orderItem action from "Existing" to "Change"
                    }
                }
                update listItems;
                if(Boolean.valueOf(inputMap.get('returnUpdatedRows'))==true){
                    outMap.put('oli',listItems);    
                }else outMap.put('oli',true);   
            }else outMap.put('oli',false);
            
            outMap.put('totalsize',listItems.size());
            Map<String, Object> errorResult = AbsServiceHandler.returnErrorResult('setValueTechSuccess',METHOD_NAME,null); 
            outMap.put('ServiceError',errorResult);  
        }
        catch(exception ex)
        {
            Map<String, Object> errorResult = AbsServiceHandler.returnErrorResult('technicalError',METHOD_NAME,ex);
            outMap.put('ServiceError',errorResult); 
        }
    }
    //End EVOL 2

    private static void preSeItemAttributeTech (SObject itemOriginal, SObject itemForUpdate, SObject itemUpdated, String sObjectApiName){
        //Update only necessary field.
        if(sObjectApiName =='OrderItem'){
           OrderItem itemForUpdateClone=(OrderItem)itemForUpdate;
           OrderItem itemOriginalClone = (OrderItem) itemOriginal;
            itemForUpdateClone.Id=itemOriginalClone.Id;
            itemForUpdateClone.vlocity_cmt__Product2Id__r=itemOriginalClone.vlocity_cmt__Product2Id__r;
            itemForUpdateClone.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c=itemOriginalClone.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c;
            itemForUpdateClone.vlocity_cmt__JSONAttribute__c = itemOriginalClone.vlocity_cmt__JSONAttribute__c;
            itemForUpdateClone.Product2 = itemOriginalClone.Product2;
            itemForUpdateClone.Product2.ProductCode = itemOriginalClone.Product2.ProductCode;
            //itemForUpdateClone.PricebookEntry = itemOriginalClone.PricebookEntry;
            itemForUpdateClone.vlocity_cmt__AttributeSelectedValues__c =itemOriginalClone.vlocity_cmt__AttributeSelectedValues__c; //for v2 Attribute model
            itemUpdated = itemForUpdateClone;
            itemOriginalClone = null;
            itemForUpdateClone = null;
        }
        if(sObjectApiName =='QuoteLineItem'){
           QuoteLineItem itemForUpdateClone=(QuoteLineItem)itemForUpdate;
           QuoteLineItem itemOriginalClone = (QuoteLineItem) itemOriginal;
            itemForUpdateClone.Id=itemOriginalClone.Id;
            itemForUpdateClone.vlocity_cmt__Product2Id__r=itemOriginalClone.vlocity_cmt__Product2Id__r;
            itemForUpdateClone.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c=itemOriginalClone.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c;
            itemForUpdateClone.vlocity_cmt__JSONAttribute__c = itemOriginalClone.vlocity_cmt__JSONAttribute__c;
            itemForUpdateClone.Product2 = itemOriginalClone.Product2;
            itemForUpdateClone.Product2.ProductCode = itemOriginalClone.Product2.ProductCode;
            //itemForUpdateClone.PricebookEntry = itemOriginalClone.PricebookEntry;
            itemForUpdateClone.vlocity_cmt__AttributeSelectedValues__c =itemOriginalClone.vlocity_cmt__AttributeSelectedValues__c; //for v2 Attribute model
            itemUpdated = itemForUpdateClone;
            itemOriginalClone = null;
            itemForUpdateClone = null;
        }
        if(sObjectApiName =='OpportunityLineItem'){
           OpportunityLineItem itemForUpdateClone=(OpportunityLineItem)itemForUpdate;
           OpportunityLineItem itemOriginalClone = (OpportunityLineItem) itemOriginal;
            itemForUpdateClone.Id=itemOriginalClone.Id;
            itemForUpdateClone.vlocity_cmt__Product2Id__r=itemOriginalClone.vlocity_cmt__Product2Id__r;
            if(itemForUpdateClone.vlocity_cmt__Product2Id__r!=null)
                itemForUpdateClone.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c=itemOriginalClone.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c;
            itemForUpdateClone.vlocity_cmt__JSONAttribute__c = itemOriginalClone.vlocity_cmt__JSONAttribute__c;
            itemForUpdateClone.Product2 = itemOriginalClone.Product2;
            itemForUpdateClone.Product2.ProductCode = itemOriginalClone.Product2.ProductCode;
            //itemForUpdateClone.PricebookEntry = itemOriginalClone.PricebookEntry;
            itemForUpdateClone.vlocity_cmt__AttributeSelectedValues__c =itemOriginalClone.vlocity_cmt__AttributeSelectedValues__c; //for v2 Attribute model
            itemUpdated = itemForUpdateClone;
            itemOriginalClone = null;
            itemForUpdateClone = null;
        }
    }

}