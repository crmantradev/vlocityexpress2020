/************************************************************************************

VPL Comms B2C { Modified some of the already existing methods and created new method

named as 'updateDefaultPhoneNumber' and also commented the rest of methods below } 

*************************************************************************************/

global without sharing class VPL_JoinAndCheckoutJourney implements vlocity_cmt.VlocityOpenInterface {



    public Boolean invokeMethod(String methodName, Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {

        Boolean result = true;

        try {

            if (methodName.equalsIgnoreCase('createJoinAcctConUser')) {

                createJoinAcctConUser(inputMap, outputMap, optionsMap);

            }

            if (methodName.equalsIgnoreCase('updateAboutYouInfo')) {

                updateAboutYouInfo(inputMap, outputMap, optionsMap);

            }

            if (methodName.equalsIgnoreCase('updateWhereYouLiveInfo')) {

                updateWhereYouLiveInfo(inputMap, outputMap, optionsMap);

            }

            if (methodName.equalsIgnoreCase('UpdateDefaultPhoneNUmber')) {

                UpdateDefaultPhoneNUmber(inputMap, outputMap, optionsMap);

            }

            if (methodName.equalsIgnoreCase('CreateSubscriptionInCRM')) {

                CreateSubscriptionInCRM(inputMap, outputMap, optionsMap);

            }

            if (methodName.equalsIgnoreCase('CreateDeviceInMatrixx')) {

                CreateDeviceInMatrixx(inputMap, outputMap, optionsMap);

            }







        } catch (Exception e) {

            outputMap.put('invokeMethod -> exception:', e);

            outputMap.put('Error line number :', e.getLinenumber());

            result = false;

        }

        return result;

    }



    /*********************************************************************************

     * createJoinAcctConUser creates account,Contact and user(next release)

     * Inputs:CartcontextId,Emailaddress,AccountNumber

     ********************************************************************************/

    public void createJoinAcctConUser(Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {



        try {



            String RecordTypeId = (String.valueOf(inputMap.get('RecordTypeId')));

            String UserType = (String.valueOf(inputMap.get('UserType')));



            Map < String, Object > GuestEmailAddressMap = new Map < String, Object > ();

            String agentProfileId = '';

            String sysAdminId;

            List < VPL_Metadata_Config__mdt > metadataList = new List < VPL_Metadata_Config__mdt > ();



            //Used metadata(VPL_Metadata_Config__mdt) for getting profileId rather than hardcoding.

            metadataList = [SELECT MasterLabel, VPL_Config_Value__c FROM VPL_Metadata_Config__mdt where VPL_Config_Value__c =: UserType];

            for (VPL_Metadata_Config__mdt mtConfig: metadataList) {

                agentProfileId = mtConfig.MasterLabel;

            }

            List < User > listOfUser = [select id from user where profileid =: agentProfileId AND USERROLEID != ''

                AND IsActive = true limit 1

            ];



            //Insert Account

            String accountNumber = (String.valueOf(inputMap.get('AccountNumber')));

            Account joinAcct = new Account();

            joinAcct.Name = 'Customer';

            joinAcct.AccountNumber = accountNumber;

            joinAcct.RecordTypeId = RecordTypeId;

            if (!listOfUser.isEmpty()) {

                joinAcct.OwnerId = listOfUser[0].Id;

            }



            insert joinAcct;



            //Insert Contact

            Contact joinCon = new Contact();

            joinCon.LastName = 'Customer';

            joinCon.AccountId = joinAcct.Id;

            joinCon.Email = (((String) inputMap.get('GuestEmailaddress') == null) ? '' : (String) inputMap.get('GuestEmailaddress'));

            insert joinCon;

            joinAcct.vlocity_cmt__PrimaryContactId__c = joinCon.Id;

            update joinAcct;



            //Update ACR

            AccountContactRelation acr = [Select Id, Roles from AccountContactRelation where accountid =: joinAcct.Id and contactid =: joinCon.Id limit 1];

            if (acr != null) {

                acr.roles = 'Owner;Payer;User';

            }

            update acr;

            outputMap.put('JoinUserAcctId', joinAcct.Id);

            outputMap.put('JoinUserConId', joinCon.Id);

            outputMap.put('ContactName', joinCon.LastName);

            system.debug('outputMap' + outputMap);



        } catch (Exception e) {

            outputMap.put('invokeMethod -> exception:', e);

            outputMap.put('Error line number :', e.getLinenumber());

            outputMap.put('assignPermissionSet', true);

        }



    }



    /*************************************************************************

     * updateAboutYouInfo update the account Name,contact Details & User Details 

     * Inputs:AccountId,ContactId,UserId

     ***************************************************************************/

    public void updateAboutYouInfo(Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {



        try {

            String custAcctId = (String) inputMap.get('CurrentUserAccountId');

            String custConId = (String) inputMap.get('CurrentUserContactId');

            //String custUserId = (String) inputMap.get('CurrentUserId');

            Map < String, Object > aboutYou = new Map < String, Object > ();

            if (inputMap.get('AboutYou') != null) {

                aboutYou = (Map < String, Object > ) inputMap.get('AboutYou');

            }

            String FirstName = (((String) aboutYou.get('FirstName') == null) ? '' : (String) aboutYou.get('FirstName'));

            String LastName = (((String) aboutYou.get('LastName') == null) ? '' : (String) aboutYou.get('LastName'));





            //Update consumer contact

            if (custAcctId != null && custAcctId != '') {

                Account acct = new Account();

                acct.Id = custAcctId;

                acct.Name = FirstName + ' ' + LastName;

                acct.vlocity_cmt__BillCycle__c = '1';

                acct.vlocity_cmt__BillFrequency__c = 'Monthly';

                acct.vlocity_cmt__Active__c = 'Yes';

                acct.vlocity_cmt__Status__c = 'Active';

                update acct;

            }



            //Update consumer contact

            if (custConId != null && custConId != '') {

                Contact con = new Contact();

                con.Id = custConId;

                con.FirstName = FirstName;

                con.LastName = LastName;

                con.OtherPhone = (((String) aboutYou.get('Phone') == null) ? '' : (String) aboutYou.get('Phone'));

                con.Salutation = (((String) aboutYou.get('Title') == null) ? '' : (String) aboutYou.get('Title'));

                con.vlocity_cmt__Status__c = 'Active';

                con.vlocity_cmt__IsActive__c = true;

                con.AccountId = custAcctId;

                update con;

            }

            outputMap.put('Exception', false);

        } catch (Exception e) {

            outputMap.put('ExceptionMessage', e);

            outputMap.put('ExceptionLineNo', e.getLinenumber());

            outputMap.put('ExceptionMethod', 'updateAboutYouInfo');

            outputMap.put('Exception', true);

        }

    }



    /*************************************************************************************

     * updateWhereYouLiveInfo update the Address to account's Billing and shipping address 

     * and contact's billing and other address 

     * Inputs:AccountId,ContactId,UserId

     *************************************************************************************/



    public void updateWhereYouLiveInfo(Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {



        try {

            String custAcctId = (String) inputMap.get('CurrentUserAccountId');

            String custConId = (String) inputMap.get('CurrentUserContactId');



            //JSON data from WhereYouLive step

            Map < String, Object > whereYouLive = new Map < String, Object > ();

            if (inputMap.get('CreditCheckWhereYouLive') != null) {

                whereYouLive = (Map < String, Object > ) inputMap.get('CreditCheckWhereYouLive');

            }

            system.debug('whereYouLive' + whereYouLive);

            Map < String, Object > MailingAddress = new Map < String, Object > ();

            MailingAddress = (Map < String, Object > ) whereYouLive.get('MailingAddress');

            system.debug('MailingAddress' + MailingAddress);

            //Assigning address into account's billing & shipping and contact's mailing and other address 

            if (!whereYouLive.isEmpty()) {

                Account consumerAcct = new Account();

                consumerAcct.Id = custAcctId;

                Contact consumerCont = new Contact();

                consumerCont.Id = custConId;

                if (whereYouLive.get('EnterCurrentAddresManually') == true) {

                    consumerAcct.ShippingStreet = consumerCont.MailingStreet = consumerAcct.BillingStreet = consumerCont.OtherStreet = ((String) MailingAddress.get('AddressLine1') == null) ? '' : (String) MailingAddress.get('AddressLine1');

                    consumerAcct.ShippingCity = consumerCont.MailingCity = consumerAcct.BillingCity = consumerCont.OtherCity = ((String) whereYouLive.get('CurrentAddressCity') == null) ? '' : (String) whereYouLive.get('CurrentAddressCity');

                    consumerAcct.ShippingState = consumerCont.MailingState = consumerAcct.BillingState = consumerCont.OtherState = ((String) whereYouLive.get('CurrentAddressState') == null) ? '' : (String) whereYouLive.get('CurrentAddressState');

                    consumerAcct.ShippingPostalCode = consumerCont.MailingPostalCode = consumerAcct.BillingPostalCode = consumerCont.OtherPostalCode = ((String) whereYouLive.get('CurrentAddressZipCode') == null) ? '' : (String) whereYouLive.get('CurrentAddressZipCode');

                }

                if (whereYouLive.get('EnterCurrentAddresManually') == false) {

                    consumerAcct.ShippingStreet = consumerCont.MailingStreet = consumerAcct.BillingStreet = consumerCont.OtherStreet = null;

                    consumerAcct.ShippingCity = consumerCont.MailingCity = consumerAcct.BillingCity = consumerCont.OtherCity = ((String) whereYouLive.get('CurrentAddressCity') == null) ? '' : (String) whereYouLive.get('CurrentAddressCity');

                    consumerAcct.ShippingState = consumerCont.MailingState = consumerAcct.BillingState = consumerCont.OtherState = ((String) whereYouLive.get('CurrentAddressState') == null) ? '' : (String) whereYouLive.get('CurrentAddressState');

                    consumerAcct.ShippingPostalCode = consumerCont.MailingPostalCode = consumerAcct.BillingPostalCode = consumerCont.OtherPostalCode = ((String) whereYouLive.get('CurrentAddressZipCode') == null) ? '' : (String) whereYouLive.get('CurrentAddressZipCode');

                }

                update consumerAcct;

                update consumerCont;

            }

        } catch (Exception e) {

            outputMap.put('ExceptionMessage', e);

            outputMap.put('ExceptionLineNo', e.getLinenumber());

            outputMap.put('ExceptionMethod', 'updateWhereYouLiveInfo');

            outputMap.put('Exception', true);

        }



    }



    /*****************************************************************************************************

     * UpdateDefaultPhoneNumber:This method provides json for updating default(free) numbers to a particular offer

     * pass the output of this method into abstraction Layer to update the numbers.

     * Inputs:GetNumberListFromInventory,GetSIMDetailsFromInventory,GetOLIDetails

     ****************************************************************************************************/

    public void UpdateDefaultPhoneNUmber(Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {



        List < Object > result1List = (List < Object > ) inputMap.get('GetNumberListFromInventory');

        List < Object > result2List = (List < Object > ) inputMap.get('GetSIMDetailsFromInventory');

        List < Object > result3List = (List < Object > ) inputMap.get('GetOLIDetails');



        OutputWrapper outWrap = new OutputWrapper();

        List < DataWrap > phoneNumber = new List < DataWrap > ();

        List < DataWrap > phoneNumberFields = new List < DataWrap > ();

        List < DataWrap > iMSI = new List < DataWrap > ();

        List < DataWrap > iCCID = new List < DataWrap > ();

        List < DataWrap > iCCIDFields = new List < DataWrap > ();

        Integer j = 0;

        Integer k = 0;

        for (Integer i = 0; i < result3List.size(); i++) {

            Map < String, Object > innerData = (Map < String, Object > ) result3List[i];

            List < Object > valuesList = (List < Object > ) innerData.get('VALUES');

            Map < String, Object > msidnData = j < result1List.size() ? (Map < String, Object > ) result1List[j] : null;

            VPL_GetInventoryDetails.siminventoryWrapper icidmsiData = k < result2List.size() ? (VPL_GetInventoryDetails.siminventoryWrapper) result2List[k] : null;

            Integer otherInfoAdded = 0;



            for (Object obj: valuesList) {

                Boolean phoneNumberAdded = false;

                Map < String, Object > valuesMap = (Map < String, Object > ) obj;

                if (valuesMap.get('TargetGroup') == 'PhoneNumber') {

                    phoneNumber.add(new DataWrap((String) innerData.get('id'), (String) msidnData.get('MSISDN')));

                    phoneNumberFields.add(new DataWrap((String) innerData.get('ParentItemId'), (String) msidnData.get('MSISDN')));

                    phoneNumberAdded = true;

                } else if (valuesMap.get('TargetGroup') == 'ICCID') {

                    iCCID.add(new DataWrap((String) innerData.get('id'), icidmsiData.ICCID));

                    iCCIDFields.add(new DataWrap((String) innerData.get('ParentItemId'), icidmsiData.ICCID));

                    otherInfoAdded++;

                } else if (valuesMap.get('TargetGroup') == 'IMSI') {

                    iMSI.add(new DataWrap((String) innerData.get('id'), icidmsiData.IMSI));

                    otherInfoAdded++;

                }

                if (otherInfoAdded == 2) {

                    k++;

                }

                if (phoneNumberAdded) {

                    j++;

                }

            }

        }

        outWrap.phoneNumber = new List < DataWrap > ();

        outWrap.iMSI = new List < DataWrap > ();

        outWrap.iCCID = new List < DataWrap > ();



        outWrap.phoneNumber = phoneNumber;

        outWrap.phoneNumberFields = phoneNumberFields;

        outWrap.iMSI = iMsi;

        outWrap.iCCID = iCCID;

        outWrap.iCCIDFields = iCCIDFields;

        String jsonInput = JSON.serialize(outWrap);

        Map < String, Object > newMap = (Map < String, Object > ) JSON.deserializeUntyped(jsonInput);

        outputMap.put('allDetails', newMap);

    }

    public class OutputWrapper {

        public List < DataWrap > phoneNumber {

            get;

            set;

        }

        public List < DataWrap > phoneNumberFields {

            get;

            set;

        }

        public List < DataWrap > iMSI {

            get;

            set;

        }

        public List < DataWrap > iCCID {

            get;

            set;

        }

        public List < DataWrap > iCCIDFields {

            get;

            set;

        }

        public OutputWrapper() {

            phoneNumber = new List < DataWrap > ();

            phoneNumberFields = new List < DataWrap > ();

            iMSI = new List < DataWrap > ();

            iCCID = new List < DataWrap > ();

            iCCIDFields = new List < DataWrap > ();

        }

    }

    public class DataWrap {

        public String ContextId {

            get;

            set;

        }

        public String Value {

            get;

            set;

        }

        public DataWrap(String ContextId, String Value) {

            this.ContextId = ContextId;

            this.Value = Value;

        }

    }



     /*************************************************************************************

      * CreateSubscriptionInCRM:This method provides json for Creating subscription in CRM 

        for all the offers.

      * pass the output of this method to LoopBlock inorder to Create subscription in matrix.

      * Inputs:AccountId,FirstName,LastName,GetNumberListFromInventory

      **************************************************************************************/  

    public void CreateSubscriptionInCRM(Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {



        //Get accountid,accountname and number details from the input map

        String AccountId = (String) inputMap.get('AccountId');

        String FirstName = (String) inputMap.get('FirstName');

        String LastName = (String) inputMap.get('LastName');

List < Object > PhoneNumList = new List < Object > ();

        PhoneNumList = (List < Object > ) inputMap.get('GetNumberListFromInventory');

        List < NumberWrapper > NumDetailslist = new List < NumberWrapper > ();

        List < Map < String, String >> rowList = new List < Map < String, String >> ();

        System.debug('PhoneNumList' + PhoneNumList);

        if (PhoneNumList.size() != null) {



            //serialize & deserilize the number details

            For(Integer i = 0; i < PhoneNumList.size(); i++) {

                NumberWrapper iw = new NumberWrapper();

                String s1 = JSON.serialize(PhoneNumList[i]);

                iw = (NumberWrapper) JSON.deserialize(s1, NumberWrapper.class);

                NumDetailslist.add(iw);

            }

        }



        if (NumDetailslist.size() != null) {

            rowList = new List < Map < String, String >> ();

            for (NumberWrapper numList: NumDetailslist) {

                Map < String, String > row = new map < String, String > ();

                row.put('FirstName', FirstName);

                row.put('LastName', LastName);

                row.put('AccountId', AccountId);

                row.put('MSISDN', numList.MSISDN);

                rowList.add(row);

            }

        }

        outputMap.put('rowList', rowList);

    }



    //Intialize wrapper class

    public class NumberWrapper {

        public String MSISDN;

    }



     /*************************************************************************************

      * CreateDeviceInMatrixx:This method provides json for Creating Subscription in Matrix 

        for all the offers.

      * pass the output of this method into a LoopBlock to create Subscription in matrix

      * Inputs: GetLIneItemDetails,ExternalId,AccountId,FirstName,FirstName,Email

      **************************************************************************************/   

    public Void CreateDeviceInMatrixx(Map < String, Object > inputMap, Map < String, Object > outputMap, Map < String, Object > optionsMap) {

        //fetching Account and LineItem Details from the input Map

        String AccountId = (String) inputMap.get('AccountId');

        String FirstName = (String) inputMap.get('FirstName');

        String LastName = (String) inputMap.get('LastName');

        String Email = (String) inputMap.get('Email');

        List < Object > LineItemList = (List < Object > ) inputMap.get('GetLIneItemDetails');

        List < Object > getExternalId = new List < Object > ();

        getExternalId = (List < Object > ) inputMap.get('ExternalId');

        //Intializing variables 

        List < Map < String, String >> rowList = new List < Map < String, String >> ();

        List < DeviceWrapper > WrapList = new List < DeviceWrapper > ();

        List < DeviceNOfferWrapper > DevWrapperList = new List < DeviceNOfferWrapper > ();

        List < DeviceNOfferWrapper > DevWrapperListnew = new List < DeviceNOfferWrapper > ();

        List < String > ExternalIdlist = new List < String > ();



        try {

            //Adding external Id to the wrapper class

            if (getExternalId.Size() != null) {

                For(Integer i = 0; i < getExternalId.size(); i++) {

                    ExternalWrapper iwExt = new ExternalWrapper();

                    String s3 = JSON.serialize(getExternalId[i]);

                    iwExt = (ExternalWrapper) JSON.deserialize(s3, ExternalWrapper.class);

                    ExternalIdlist.add(iwExt.ExternalId);

                }

            }

           //Fetching 'VALUES' Node values and adding it into the wrapper class.

            if (LineItemList.Size() != null) {

                For(Integer i = 0; i < LineItemList.size(); i++) {

                    Map < String, Object > innerData = (Map < String, Object > ) LineItemList[i];

                    List < Object > valuesList = (List < Object > ) innerData.get('VALUES');

                    DeviceWrapper DevWrap = new DeviceWrapper();

                    WrapList = new List < DeviceWrapper > ();

                    for (Object obj: valuesList) {

                        VALUES iwswrap = new VALUES();

                        String s2 = JSON.serialize(obj);

                        iwswrap = (VALUES) JSON.deserialize(s2, VALUES.class);

                        if (iwswrap.TargetGroup == 'PhoneNumber') {

                            DevWrap.phoneNumber = iwswrap.Value;

                        } else if (iwswrap.TargetGroup == 'SubscriptionNumber') {

                            DevWrap.SubscriptionNumber = iwswrap.Value;

                        } else if (iwswrap.TargetGroup == 'ICCID') {

                            DevWrap.ICCID = iwswrap.Value;

                        } else if (iwswrap.TargetGroup == 'IMSI') {

                            DevWrap.IMSI = iwswrap.Value;

                        }

                    }

                    WrapList.add(DevWrap);

                    for (DeviceWrapper sim: WrapList) {

                        DeviceNOfferWrapper valsim = new DeviceNOfferWrapper();

                        valsim.phoneNumber = sim.phoneNumber;

                        valsim.subscriptionNumber = sim.subscriptionNumber;

                        valsim.IMSI = sim.IMSI;

                        valsim.ICCID = sim.ICCID;

                        valsim.subscriptionNumber = sim.subscriptionNumber;

                        if (valsim.phoneNumber != null && valsim.subscriptionNumber != null) {

                            DevWrapperList.add(valsim);

                        } else if (valsim.IMSI != null && valsim.ICCID != null) {

                            DevWrapperListnew.add(valsim);

                        }

                    }

                }

            }

            for (DeviceNOfferWrapper DevList: DevWrapperListnew) {

                for (DeviceNOfferWrapper offerList: DevWrapperList) {

                    if (DevList.SubscriptionNumber == offerList.SubscriptionNumber) {

                        DevList.phoneNumber = offerList.phoneNumber;

                        DevList.AccountId = AccountId;

                        DevList.FirstName = FirstName;

                        DevList.LastName = LastName;

                        DevList.Email = Email;

                    }

                }

            }

            for (DeviceNOfferWrapper newlist: DevWrapperListnew) {

                integer j = 0;

                newlist.MatrixxOfferCode = ExternalIdlist[j];

                j++;

            }

            //Adding all the values from the wrapper class to map

                if (DevWrapperListnew.Size() != null) {

                rowList = new List < Map < String, String >> ();

                for (DeviceNOfferWrapper SIMList: DevWrapperListnew) {

                    Map < String, String > row = new map < String, String > ();

                    row.put('FirstName', SIMList.FirstName);

                    row.put('LastName', SIMList.LastName);

                    row.put('Email', SIMList.Email);

                    row.put('AccountId', SIMList.AccountId);

                    row.put('MSISDN', SIMList.phoneNumber);

                    row.put('ICCID', SIMList.ICCID);

                    row.put('IMSI', SIMList.IMSI);

                    row.put('SubscriptionNumber', SIMList.SubscriptionNumber);

                    row.put('MatrixxOfferCode', SIMList.MatrixxOfferCode);

                    rowList.add(row);

                }

            }

            outputMap.put('rowList', rowList);



        } catch (Exception e) {

            outputMap.put('invokeMethod -> exception:', e);

            outputMap.put('Error line number :', e.getLinenumber());

        }

    }



    //Intializing all the wrapper classes

    public class VALUES {

        public String TargetType;

        public String TargetGroup;

        public String IntegrationKey;

        public String Name;

        public String Value;

    }

    public class DeviceWrapper {

        public String phoneNumber;

        public String IMSI;

        public String ICCID;

        public String Value;

        public String SubscriptionNumber;

    }

    public class DeviceNOfferWrapper {

        public String phoneNumber;

        public String iMSI;

        public String iCCID;

        public String SubscriptionNumber;

        public String AccountId;

        public String FirstName;

        public String LastName;

        public String Email;

        public String MatrixxOfferCode;

    }

    public class ExternalWrapper {

        public String ExternalId;

    }

}