global class VPL_EligibilityRuleController implements vlocity_cmt.VlocityOpenInterface2 {
    global static Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outMap, Map<String, Object> options) {
        Boolean result = false;
        try{
            if(methodName.equalsIgnoreCase('launchVIPS')){
                launchVIPS(inputMap, outMap);
            }
            else{
                result = false;
            }
        }
        catch(Exception e){
            result = false;
        }
        return result;
    } 

    public static void launchVIPS(Map<String,Object> inputMap, Map<String,Object> outMap){
    
        String ruleFormula = (String) inputMap.get('Rule');
        List<String> rules = ruleFormula.split(' ');
        List<String> vips = new List<String>();
        List<String> operators = new List<String>();

        Integer i = 0;
        for(String rule: rules) {
            if(math.mod(i++, 2)==0) {
                vips.add(rule);
            }
            else {
                operators.add(rule);
            }
        }

        List<String> totals = new List<String>();
        List<Map<String,String>> reasonsmap = new List<Map<String,String>>();
  
        for (Integer j = 0; j < vips.size(); j++) {
            Map<String, Object> ipOutput = (Map <String, Object>) vlocity_cmt.IntegrationProcedureService.runIntegrationService(vips.get(j), inputMap, new Map<String, Object>{'isDebug' => true});
            Map<String, Object> result = (Map<String, Object>) ipOutput.get('ResponseAction');
           
            if (result.get('result').equals('PASS')) {
                totals.add('TRUE');
            }
            else {
                totals.add('FALSE');
            }
            if((String)result.get('reason') != '' && (String)result.get('reason') != null){
                reasonsmap.add(new Map<String, String>{'rule' =>vips.get(j), 'reason'=>(String)result.get('reason')});
           }
        }
        
        String rule2check = '';
        for (Integer x = 0; x < vips.size(); x++) {
            rule2check += totals.get(x) + ' ';
            if (x != vips.size()-1){
                rule2check += operators.get(x) + ' ';
            }
        }
        rule2check = rule2check.trim();
        Boolean finalCheck = VPL_BooleanExpression.eval(rule2check);

        outMap.put('isEligible', finalCheck);
        outMap.put ('ineligibilityReasons', reasonsmap);
    }
}