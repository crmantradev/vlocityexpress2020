global with sharing class AbsServiceHandler implements vlocity_cmt.VlocityOpenInterface {
/*********************************************************************************************
     * @Class AbsServiceHandler
     * @Description Abstraction Service to manage
     *              - getMetadata
     *              - MatchData
     *              - Any Generic fuctionality
     * @Created Alain KAMENI, June 2020
     **********************************************************************************************/
    static final String ENTITY_CONTEXT_ROOT = 'Root';//Root Context
    static final String ENTITY_CONTEXT_ROOTCHILD = 'RootChild';//RootChild Context
    static final String ENTITY_CONTEXT_CHILD = 'Child';//Child Context
    static final String MODE_AUTO = 'auto'; //auto mode
    static final String MODE_DIRECTCHILD = 'directChild'; //directChild (used in getData)
    static Integer countdepth;    
    static String METHOD_NAME = '';

    
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean result = true;
        String errormessage ='';
        try
        {
            METHOD_NAME = methodName;
            if (methodName.equals('getMappingData')){
                getMappingData(inputMap,outMap,options); 
            }else if (methodName.equals('getMetadataRecords')){
                getMetadataRecords(inputMap,outMap,options);  
            }else if (methodName.equals('warmUpService')){
                warmUpService(inputMap,outMap,options);  
            }else{
                result = false;
            } 
        } 
        catch(Exception e)
        {
            System.debug('invokeMethod ERROR is ->: '+e.getMessage());
            System.debug('invokeMethod -> exception: '+e.getStackTraceString());
            result = false;
            Map<String, Object> errorResult = returnErrorResult('technicalError',METHOD_NAME,e);
            outMap.put('ServiceError',errorResult); 
        }
        return result;
    }
      
    global static string invokejsonSupport(SObject OItm,String jsonstr)
    {
        Map<String,Object> outputMap = new Map<String,Object>();
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> optionsMap = new Map<String,Object>();
        List<Vlocity_cmt.JSONAttributeSupport.JSONAttributeActionRequest> actionRequestsList = new List<Vlocity_cmt.JSONAttributeSupport.JSONAttributeActionRequest>();
        Object variableJsonObj   = jsonstr != null ? JSON.deserializeUntyped(jsonstr) : null;
        
        if(variableJsonObj instanceof Map<String,Object>)
        { 
            Map<String,Object> variableJsonMap = (Map<String,Object>)variableJsonObj;
            for(String k: variableJsonMap.keyset())
            { 
                actionRequestsList.add(new vlocity_cmt.JSONAttributeSupport.JSONAttributeActionRequest(k, vlocity_cmt.JSONAttributeSupport.ActionType.ASSIGN, variableJsonMap.get(k)));
            }
        }
        vlocity_cmt.JSONAttributeSupport jsonSupport = new vlocity_cmt.JSONAttributeSupport();
        inputMap.put('objectSO', null);
        if(OItm.get('vlocity_cmt__JSONAttribute__c') !=null && OItm.get('vlocity_cmt__AttributeSelectedValues__c') ==null){ //v1 Attribute Model
            inputMap.put('runTimeAttributesJSON', OItm.get('vlocity_cmt__JSONAttribute__c'));
            //inputMap.put('originalAttributesJSON', ((OItm.getSObject('PricebookEntry')).getSObject('Product2')).get('vlocity_cmt__JSONAttribute__c')); 
            inputMap.put('originalAttributesJSON', OItm.getSObject('Product2').get('vlocity_cmt__JSONAttribute__c')); 
        }else if(OItm.get('vlocity_cmt__AttributeSelectedValues__c') !=null){ //v2 Attribute Model
            inputMap.put('runTimeAttributesValuesJSON', OItm.get('vlocity_cmt__AttributeSelectedValues__c'));
            inputMap.put('originalAttributesMetadata', OItm.getSObject('Product2').get('vlocity_cmt__AttributeMetadata__c')); 
        }
        inputMap.put('JSONAttributeActionRequestList', actionRequestsList);
        jsonSupport.invokeMethod('applyAttributeActions', inputMap, outputMap, optionsMap);
        if(OItm.get('vlocity_cmt__JSONAttribute__c') !=null && OItm.get('vlocity_cmt__AttributeSelectedValues__c') ==null){ //v1 Attribute Model
            return (String)outputMap.get('modifiedJSON');
        }else if(OItm.get('vlocity_cmt__AttributeSelectedValues__c') !=null){ //v2 Attribute Model
           return (String)outputMap.get('attributesValues'); 
        }else return null;
    }
    
    public static void cpqappHandlerMethod( SObject oli, id OrdId, Map <String,Object> inputMap, Map <String,Object> outMap,string mod_json, Map<String,String> prodIdPbeId, SObject parent_ord_item)
    {  
            vlocity_cmt.CPQAppHandler app = new vlocity_cmt.CPQAppHandler ();
            Map<String,Object> itemsMap = new Map<String,Object>();
            Map<String,Object>input = new Map<String,Object>();
            Map<String,Object>output = new Map<String,Object>();
            Map<String,Object>options= new Map<String,Object>();
            input.put('cartId', ordId);
            
            input.put('fields','vlocity_cmt__JSONAttribute__c, Quantity,vlocity_cmt__AttributeSelectedValues__c');
            
            list<object> product_obj_info = updatefieldMapJson(mod_json, oli,new map<string,string>{(String)oli.get('Product2Id') => (String) prodIdPbeId.get((ID)oli.get('Product2Id')) },null);
            if (oli.get('vlocity_cmt__ParentItemId__c') != null){
                if (product_obj_info.size() >0){
                    system.debug(prodIdPbeId + ' ' + parent_ord_item.get('Product2Id') );
                    list<object>  product_obj_info_parent = updatefieldMapJson(mod_json, oli,new map<string,string>{(String)parent_ord_item.get('Product2Id') => (String)prodIdPbeId.get((ID)parent_ord_item.get('Product2Id')) },null);
                    Map<String,Object> updatefieldMap =(Map<String,Object>)product_obj_info_parent[0];
                    updatefieldMap.put ('lineItems',  new Map<String, Object>{'records' => product_obj_info});
                    itemsMap.put('records', product_obj_info_parent);
                    input.put('hierarchy',-1);
                    
                }
            }else {
                itemsMap.put('records', product_obj_info);
            }

            input.put('items',itemsMap);
            outMap.put('inputputCartsItemsAPI',input); 
    }
    public static list<object> updatefieldMapJson(String modifiedJson, SObject oli, Map<String,String> prodIdPbeId ,String newmsidnValue)
    {
        System.debug('modifiedJson===>'+modifiedJson);
        System.debug('prodIdPbeId===>'+prodIdPbeId);
        List<Object> childrecordList = new List<Object>();
        vlocity_cmt.VOIInvoker voi = vlocity_cmt.VOIInvoker.getInstance();
        Map<String,Object> updatechildfieldMap = new Map<String,Object>();
        Map<String,Object> product2Map = new Map<String,Object>();
       
        vlocity_cmt.JSONField idField = (vlocity_cmt.JSONField)voi.invoke('JSONField', 'debugCreate',null, null, null);
        //id of the order product
         idField.value = oli.Id;
         updatechildfieldMap.put('Id', idField);

        vlocity_cmt.JSONField quantity = (vlocity_cmt.JSONField)voi.invoke('JSONField', 'debugCreate',null, null, null);
        quantity.value = oli.get('Quantity');
        quantity.dataType = 'double';
        updatechildfieldMap.put('Quantity', quantity);

        product2Map.put('Id',oli.get('vlocity_cmt__Product2Id__c'));
        updatechildfieldMap.put('Product2',product2Map);
        
        Map<String,Object> pbeMap = new Map<String,Object>();
        pbeMap.put('Product2',product2Map);
        //price book entry id of the item being modified
        pbeMap.put('Id',prodIdPbeId.get((ID)oli.get('vlocity_cmt__Product2Id__c')));
        updatechildfieldMap.put('PricebookEntry',pbeMap);
   
        vlocity_cmt.JSONField hierarchyPath = (vlocity_cmt.JSONField)voi.invoke('JSONField', 'debugCreate',null, null, null);
        hierarchyPath.value = oli.get('vlocity_cmt__ProductHierarchyPath__c');
        hierarchyPath.dataType = 'string';
        updatechildfieldMap.put('vlocity_cmt__ProductHierarchyPath__c', hierarchyPath);

        if(oli.get('vlocity_cmt__JSONAttribute__c') !=null && oli.get('vlocity_cmt__AttributeSelectedValues__c') ==null){
            vlocity_cmt.JSONField jsonAttribute = (vlocity_cmt.JSONField)voi.invoke('JSONField', 'debugCreate',null, null, null);
            jsonAttribute.value = modifiedJson;
            jsonAttribute.dataType = 'string';
            updatechildfieldMap.put('vlocity_cmt__JSONAttribute__c', jsonAttribute);
        }else if(oli.get('vlocity_cmt__AttributeSelectedValues__c') !=null){
            vlocity_cmt.JSONField attributeSelectedValues = (vlocity_cmt.JSONField)voi.invoke('JSONField', 'debugCreate',null, null, null);
            attributeSelectedValues.value = modifiedJson;
            attributeSelectedValues.dataType = 'string';
            updatechildfieldMap.put('vlocity_cmt__AttributeSelectedValues__c', attributeSelectedValues);
        }
 
        updatechildfieldMap.put('itemType', 'lineItem');  
        childrecordList.add(updatechildfieldMap);
        return childrecordList;

    }
   //#EVOL : method getContextByMode
    private Map <String, String> getContextByMode(Map<String,Object> inputMap){
        System.debug('#Start getContextByMode method');
        String contextId = ((String)inputMap.get('ContextId')==null) ? '': (String)inputMap.get('ContextId');
        String entity;
        Map <String,String> mapContextData;
        String sObjectApiName = contextId==''?null:String.valueOf(((ID) contextId).getSObjectType()); // get type or primary sObject (Order, Quote, Opportunity)
        if((String) inputMap.get('Mode') == MODE_DIRECTCHILD){ //Start mode == Auto
            //get ParentEntityContext (selectedContext - Context of ContextRecord selected)
            if(inputMap.get('SelectedContext') != null && inputMap.get('SelectedContext') !=''
              && inputMap.get('SelectedEntity') != null && inputMap.get('SelectedEntity') !=''
              ){
                mapContextData = new Map <String,String>{'context' => (String) inputMap.get('SelectedContext'),'entity' => (String) inputMap.get('SelectedEntity')};
            }else{ // Fecth Context by Query ContextRecord
                List<SObject> contextRecordList;
                if(sObjectApiName =='OrderItem'){
                    contextRecordList  = [select Id,vlocity_cmt__RootItemId__c ,vlocity_cmt__ParentItemId__c, Product2.ProductCode from OrderItem where Id= :contextId];
                }else if(sObjectApiName =='QuoteLineItem'){
                    contextRecordList  = [select Id,vlocity_cmt__RootItemId__c ,vlocity_cmt__ParentItemId__c, Product2.ProductCode from QuoteLineItem where Id= :contextId];  
                }else if(sObjectApiName =='OpportunityLineItem'){
                    contextRecordList  = [select Id,vlocity_cmt__RootItemId__c ,vlocity_cmt__ParentItemId__c, Product2.ProductCode from OpportunityLineItem where Id= :contextId];  
                }
                
                SObject contextRecord = contextRecordList!=null && contextRecordList.size()>0 ? contextRecordList[0]:null;             
                if(contextRecord !=null){
                    mapContextData = new Map <String,String>();
                    if(contextRecord.get('vlocity_cmt__ParentItemId__c') == NULL){
                        mapContextData.put('context',ENTITY_CONTEXT_ROOT);
                    }else if(contextRecord.get('vlocity_cmt__ParentItemId__c') != NULL && contextRecord.get('vlocity_cmt__ParentItemId__c') == contextRecord.get('vlocity_cmt__RootItemId__c') ){
                      mapContextData.put('context',ENTITY_CONTEXT_ROOTCHILD);
                    }else if(contextRecord.get('vlocity_cmt__ParentItemId__c') != NULL && contextRecord.get('vlocity_cmt__ParentItemId__c') != contextRecord.get('vlocity_cmt__RootItemId__c') ){
                        mapContextData.put('context',ENTITY_CONTEXT_CHILD);
                    }
                    mapContextData.put('entity',(String)(contextRecord.getSObject('Product2')).get('ProductCode'));
                }
            }// end else
        } // End Mode : Auto
        System.debug('##End getContextByMode');
        return mapContextData;
    }
    /*Use for GetData in Abstraction Service */
    global void getMetadataRecords(Map<String,Object> inputMap,Map<String,Object> outMap,Map<String,Object> options){
        List<Object> targetValues = new List<Object>();
        if(inputMap.get('FilterField') != null){
           targetValues = (List<Object>)inputMap.get('FilterField');
        }  
        List<string> criteria = new list<string>();
        System.debug('targetValues--->'+targetValues);
        String whereClause = '';       
        //Context: Start auto get Metadata record
        Boolean stopProcess = false;
        String mode = (String)inputMap.get('Mode');
        if (mode == MODE_DIRECTCHILD){
            Map <String,String> mapContextData = getContextByMode(inputMap);
            if(mapContextData != null && mapContextData.get('context') != null && mapContextData.get('entity') != null){ 
                if(targetValues !=null){ // update FilterField
                    targetValues.add(new Map<String,String>{'Name'=> 'ParentEntityContext', 'Value'=> mapContextData.get('context')}); //EVOL2 : move space on name
                    if(mapContextData.get('context') != ENTITY_CONTEXT_ROOT)//Add only when Context is not Root
                        targetValues.add(new Map<String,String>{'Name'=> 'ParentEntity', 'Value'=> mapContextData.get('entity')}); //EVOL2: move space on name
                }
            }else{
                stopProcess = true;
                String reasonStopProcess = 'Context not Found for Mode: '+inputMap.get('Mode');
                targetValues = null;
            }
        } //Context: End auto get Metadata record
        if(!Boolean.valueOf(inputMap.get('IgnoreActiveFlag'))) whereClause = 'VCPS_Active__c=true'; // check active 
        if(targetValues != null){    
            for(Object obj : targetValues){    
                Map<String,Object> data = (Map<String,Object>)obj;
                String fieldName = 'VCPS_'+ String.valueOf(data.get('Name')) +  '__c'; //#BUG: Fixed to avoid issue with contains and "OR" where clause
                String fieldValue = String.valueOf(data.get('Value'));               
                if(fieldName != '' && fieldValue != ''){
                   if(whereClause != '' && whereClause.contains(fieldName)){
                        whereClause += ' OR ';
                    }else if(whereClause != ''){
                        whereClause += ' AND ';
                    }
                   whereClause += String.escapeSingleQuotes(fieldName) + ' =\'' + String.escapeSingleQuotes(fieldValue) + '\'';
                }
                
            }
        }
     
        List<Object> resultDataJson = new List<Object>();
        List<VCPS_Abstraction_Layer_Setup__mdt> resultData = new List<VCPS_Abstraction_Layer_Setup__mdt>();
        // querying data from custom metadata
        if(whereClause != ''){
            String soqlString = 'SELECT Label,VCPS_Active__c,VCPS_Flow__c,VCPS_TargetEntity__c, VCPS_ParentEntity__c, VCPS_Target__c, VCPS_TargetType__c, VCPS_IntegrationKey__c, VCPS_TargetEntityContext__c, VCPS_ParentEntityContext__c, VCPS_TargetGroup__c,VCPS_TargetDataType__c from VCPS_Abstraction_Layer_Setup__mdt where ' + whereClause;
            resultData = Database.query(soqlString);
            System.debug('resultData--->'+resultData);
            resultDataJson = (List<Object>)JSON.deserializeUntyped(JSON.serialize(resultData));
        }
        
        if(resultDataJson.size() > 0){
            outMap.put('QueryResult',resultDataJson); // returning result and success
            Map<String, Object> errorResult = returnErrorResult('getDataSuccess',METHOD_NAME,null);
            outMap.put('ServiceError',errorResult);     
        }else{
            // returning error
            if(stopProcess && mode == MODE_DIRECTCHILD){ // #EVOL Context - Error Handling
                Map<String, Object> errorResult = returnErrorResult('getDataFailureContext',METHOD_NAME,null);
                outMap.put('ServiceError',errorResult);   
            }else{ 
                Map<String, Object> errorResult = returnErrorResult('getDataFailure',METHOD_NAME,null);
                outMap.put('ServiceError',errorResult); 
            }
       }     
}

/*Use for MatchData in Abstraction Service */
global void getMappingData(Map<String,Object> inputMap,Map<String,Object> outMap,Map<String,Object> options){
    Object filterObj; 
    List<Object> filterList = new List<Object>();
    List<Object> tempFilterList = new List<Object>();
    if(inputMap.get('AbstractServiceResult') != null && inputMap.get('AbstractServiceResult') != ''){
       if(inputMap.get('AbstractServiceResult') instanceof List<Object>){
       filterList = (List<Object>)inputMap.get('AbstractServiceResult');
       }else{
           filterList.add(inputMap.get('AbstractServiceResult'));
       } 
    }else{
        filterList = null;
    }
    List<Object> listAttributeMapping = new List<Object>();
    Map<String,Object> mapIntegrationKeyValue = new Map<String,Object>();
    List<Object> attributeValueList;
    String matchField;
    map <String,String> mapFieldNameBymatchField = new map <String,String> ();
    if(inputMap.get('AttributeValueGroup') != null && inputMap.get('AttributeValueGroup') !=''){ //#EVOL : add
        if(inputMap.get('AttributeValueGroup') instanceof List<Object>)
            attributeValueList = (List<Object>)inputMap.get('AttributeValueGroup');
        else attributeValueList.add(inputMap.get('AttributeValueGroup'));
        matchField = 'TargetGroup';
        mapFieldNameBymatchField.put(matchField,'VCPS_TargetGroup__c');
    }
    else if(inputMap.get('AttributeValue') != null && inputMap.get('AttributeValue') !=''){
        //attributeValueList = inputMap.get('AttributeValue') ==''?null: (List<Object>)inputMap.get('AttributeValue');
        if(inputMap.get('AttributeValue') instanceof List<Object>)
            attributeValueList = (List<Object>)inputMap.get('AttributeValue');
        else attributeValueList.add(inputMap.get('AttributeValue'));
        matchField = 'IntegrationKey';
        mapFieldNameBymatchField.put(matchField,'VCPS_IntegrationKey__c');
    }
    // End #EVOL
     //End
    if(attributeValueList != null){
        System.debug('attributeValueList--->'+attributeValueList);
        for(Object obj : attributeValueList){    
            Map<String,Object> data = (Map<String,Object>)obj;   
            String intKey = String.valueOf(data.get(matchField));
                Object attValue = data.get('Value');
                mapIntegrationKeyValue.put(intKey,attValue);
        }
    }
    System.debug('mapIntegrationKeyValue--->'+mapIntegrationKeyValue);
    System.debug('filterList--->'+filterList);
   
    if(filterList !=null){
 
      for(Object obj : filterList){
        DataWrapper wrapObj = new DataWrapper();
        Map<String,Object> data = (Map<String,Object>)obj;
        System.debug('data--->'+data);
        System.debug('condition check--->'+mapIntegrationKeyValue.keySet().contains(String.valueOf(data.get('VCPS_IntegrationKey__c'))));
        if(mapIntegrationKeyValue.keySet().size() > 0 && mapIntegrationKeyValue.keySet().contains(String.valueOf(data.get(mapFieldNameBymatchField.get(matchField))))){
            //returning data 
            wrapObj.Name = String.valueOf(data.get('VCPS_Target__c'));
            if(String.valueOf(data.get('VCPS_TargetType__c')) == 'Attribute' || String.valueOf(data.get('VCPS_TargetType__c')) == 'Field'){ //#EVOL: Add Field - to manage setFieldValue operation
             wrapObj.Value = mapIntegrationKeyValue.get(String.valueOf(data.get(mapFieldNameBymatchField.get(matchField))));
            }
            wrapObj.TargetEntity = String.valueOf(data.get('VCPS_TargetEntity__c'));
            wrapObj.ParentEntity = String.valueOf(data.get('VCPS_ParentEntity__c'));
            wrapObj.TargetEntityContext = String.valueOf(data.get('VCPS_TargetEntityContext__c'));
            wrapObj.TargetType = String.valueOf(data.get('VCPS_TargetType__c'));
            wrapObj.ParentEntityContext = String.valueOf(data.get('VCPS_ParentEntityContext__c'));
            wrapObj.IntegrationKey = String.valueOf(data.get('VCPS_IntegrationKey__c'));
            wrapObj.TargetGroup = String.valueOf(data.get('VCPS_TargetGroup__c'));
            wrapObj.TargetGroup = String.valueOf(data.get('VCPS_TargetGroup__c'));  //EVOL#3 :add
            listAttributeMapping.add(wrapObj);
             
        }else if(mapIntegrationKeyValue.keySet().size() == 0){
            wrapObj.Name = String.valueOf(data.get('VCPS_Target__c'));
            wrapObj.TargetEntity = String.valueOf(data.get('VCPS_TargetEntity__c'));
            wrapObj.ParentEntity = String.valueOf(data.get('VCPS_ParentEntity__c'));
            wrapObj.TargetEntityContext = String.valueOf(data.get('VCPS_TargetEntityContext__c'));
            wrapObj.TargetType = String.valueOf(data.get('VCPS_TargetType__c'));
            wrapObj.ParentEntityContext = String.valueOf(data.get('VCPS_ParentEntityContext__c'));
            wrapObj.IntegrationKey = String.valueOf(data.get('VCPS_IntegrationKey__c'));
            wrapObj.TargetGroup = String.valueOf(data.get('VCPS_TargetGroup__c')); //EVOL#3 :add
            wrapObj.TargetDataType = String.valueOf(data.get('VCPS_TargetDataType__c')); //EVOL#2 Add TargetDataType for Field Update
            listAttributeMapping.add(wrapObj);
        }     
      }  
    }
    
    if(listAttributeMapping.size() > 0){
        Map<String, Object> errorResult = returnErrorResult('getDataSuccess',METHOD_NAME,null); 
        outMap.put('ServiceError',errorResult);  
    }else{ // returning error
        Map<String, Object> errorResult = returnErrorResult('getDataFailure',METHOD_NAME,null);
        outMap.put('ServiceError',errorResult);     
    }
    System.debug('listAttributeMapping--->'+listAttributeMapping);
    listAttributeMapping = (List<Object>)JSON.deserializeUntyped(JSON.serialize(listAttributeMapping));
    outMap.put('ListAttributes',listAttributeMapping);
}

    /*
        Description: Wrapper class for setting abstraction layer data for setAttributes/add/disconnect products
    */
    global class DataWrapper{
        String Name;
       // String Value; //EVOL MUL
       Object Value;
        String TargetEntity;
        String ParentEntity;
        String TargetEntityContext;
        String TargetType;
        String ParentEntityContext;
        String IntegrationKey;
        String TargetGroup;
        String TargetDataType;
        global dataWrapper(){
            Name = '';
            TargetEntity = '';
            ParentEntity = '';
            TargetEntityContext = '';
            TargetType = '';
            ParentEntityContext = '';
            IntegrationKey = '';
            TargetDataType = ''; //EVOL#2 Add TargetDataType for Field Update
        }
    }
    /* ** Preload Apex Classes for first run issue ** */
    global void warmUpService(Map<String,Object> inputMap,Map<String,Object> outMap,Map<String,Object> options){

        List<String> listAbstractionClasses = inputMap.get('listAbstractionClasses')!=''?String.valueof(inputMap.get('listAbstractionClasses')).split(','):null;
        List<String> listIndividualClasses = inputMap.get('listIndividualClasses')!=''?String.valueof(inputMap.get('listIndividualClasses')).split(','):null;
        List<String> warmUpIndividualClasses = new List<String>();
        List<String> listClassesIn = new List<String>();
        
        if(Boolean.valueof(inputMap.get('warmUpOnlyIndividualClasses'))){
            if(listIndividualClasses !=null && listIndividualClasses.size()>0) listclassesIn.addAll(listIndividualClasses);
        }else{ //Abstraction classes
            if(listAbstractionClasses !=null && listAbstractionClasses.size()>0) listClassesIn.addAll(listAbstractionClasses);
            if(listIndividualClasses !=null && listIndividualClasses.size()>0) listClassesIn.addAll(listIndividualClasses);
        } 
        if(listClassesIn.size()>0){
           for(String clsIn : listClassesIn){
             Type.forName(clsIn).newInstance();
             if(!listAbstractionClasses.contains(clsIn)) warmUpIndividualClasses.add(clsIn);
           }
           outMap.put('endTime',datetime.now());
           if(warmUpIndividualClasses.size()>0) outMap.put('warmUpIndividualClasses',warmUpIndividualClasses);
        } 
    }
    /*** End warmUpService ***/
    global static Map<String,Object> returnErrorResult(String methodName, String parentMethodName,Exception e){
        Map<String, String> theObj = new Map<String, String>();
        if(methodName == 'technicalError' && e != null){ // returing catch exception
                theObj.put('ErrorCode','99999');
                theObj.put('ErrorMessage','Technical error occurred in method: '+parentMethodName+ ' @ linenumber '+e.getLineNumber()+ ' Exception detail: '+ e.getMessage());
        }else if(methodName == 'getDataSuccess'){ // returing success for get/match data 
            theObj.put('ErrorCode','00000');
            theObj.put('ErrorMessage',parentMethodName + ' OK: Data Found');
        }else if(methodName == 'getDataFailure'){ // returing failure for get/match data 
            theObj.put('ErrorCode','00001');
            theObj.put('ErrorMessage',parentMethodName + ' FAILURE: Data not found');
        }else if(methodName == 'preProcessProductSuccess'){ // returing success for preProcessProduct 
            theObj.put('ErrorCode','00000');
            theObj.put('ErrorMessage',parentMethodName + ' OK');
        }else if(methodName == 'preProcessProductFailure'){ // returing failure for preProcessProduct 
            theObj.put('ErrorCode','00001');
            theObj.put('ErrorMessage',parentMethodName + ' NOT OK');
        }else if(methodName == 'preProcessAttributesSuccess'){ // returing failure for preProcessProduct 
            theObj.put('ErrorCode','00000');
            theObj.put('ErrorMessage',parentMethodName + ' OK');
        }else if(methodName == 'preProcessAttributesFailure'){ // returing failure for preProcessProduct 
            theObj.put('ErrorCode','00001');
            theObj.put('ErrorMessage',parentMethodName + ' NOTHING FOUND');
        }else if(methodName == 'getDataFailureContext'){ // #EVOL Context - ErrorHandling Context not found
            theObj.put('ErrorCode','00002');
            theObj.put('ErrorMessage',parentMethodName + ' CONTEXT NOT FOUND'); 
        }else if(methodName == 'setValueTechSuccess'){ // returing success for setValueTech
            theObj.put('ErrorCode','00000');
            theObj.put('ErrorMessage',parentMethodName + ' OK');
        }else if(methodName == 'getValueSuccess'){ // returing success for getValue
            theObj.put('ErrorCode','00000');
            theObj.put('ErrorMessage',parentMethodName + ' OK');
        }else if(methodName == 'getValueFailureNotFound'){ // returing success for getValue
            theObj.put('ErrorCode','00001');
            theObj.put('ErrorMessage',parentMethodName + ' NOTHING FOUND');
        }
   
        return (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(theObj));
    }

 
}